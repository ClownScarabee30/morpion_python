from Constante import *

def creeIA()->dict:
    j = dict
    j[NAME] = None
    j[SIGNE] = "_"

    return j

def ordreDeJeu()->bool:
    rep = int(input("Voulez vous jouez en premier ? O/N"))
    repp = None
    while rep.upper() != "O" and rep.upper()!= "N":
        print("Réponse incorrecte \nMerci de répondre par 'O' ou 'N'")
        rep = int(input("Voulez vous jouez en premier ? O/N"))
    if rep.upper() == "O":
        repp = True
    elif rep.upper() == "N":
        repp = False

    return repp

