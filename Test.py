def testIndex(valeur: list,index : int)-> bool:
    #vérifie si l'index indiqué d'une liste est compris (TRUE) ou non (FALSE)
    rep = False
    if index >= 0 and index < len(valeur):
        rep = True
    return rep