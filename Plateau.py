from Test import *
from Constante import *

def creerValeur()->list: #génération de la liste qui contiendra les valeurs
    value = []
    for i in range(9):
        value.append("_")

    return value


def setValeur(value: list, index:int, j: dict)-> list:
    index -= 1
    while testIndex(value, index) == False or value[index] != "_":
        print("ERREUR setValeur : Le numéro indiqué ne correspond à aucune case ou est déjà occupé")
        index = int(input("Choississez un nombre entre 1 et 9"))
        index -= 1
    if j[SIGNE] != "X" and j[SIGNE] != "O":
        raise ValueError("setValeur : Le signe du joueur ne convient pas")

    value[index] = j[SIGNE]
    return value

def genererPlateau(value: list)->None:

    a = 0

    for i in range(3):

        print(" ",value[0+a]," | ",value[1+a]," | ",value[2+a])

        a += 3
        if i == 0 or i == 1:
            print("-----+-----+-----")
    return None

def gagnant(plateau : list)-> bool:
    rep = False
    if plateau[0] == plateau[1] == plateau[2] != "_" or plateau[3] == plateau[4] == plateau[5] != "_" or plateau[6] == plateau[7] == plateau[8] != "_" or plateau[0] == plateau[3] == plateau[6] != "_" or plateau[1] == plateau[4] == plateau[7] != "_" or plateau[2] == plateau[5] == plateau[8] != "_" or plateau[0] == plateau[4] == plateau[8] != "_" or plateau[2] == plateau[4] == plateau[6] != "_":
        rep = True

    return rep

def getSigneGagnant(plateau:list)->str:
    rep = ""

    if plateau[0] == plateau[1] == plateau[2] != "_" :
        rep = plateau[0]
    elif plateau[3] == plateau[4] == plateau[5] != "_" :
        rep = plateau[3]
    elif plateau[6] == plateau[7] == plateau[8] != "_" :
        rep = plateau[6]
    elif plateau[0] == plateau[3] == plateau[6] != "_" :
        rep = plateau[0]
    elif plateau[1] == plateau[4] == plateau[7] != "_" :
        rep = plateau[1]
    elif plateau[2] == plateau[5] == plateau[8] != "_" :
        rep = plateau[2]
    elif plateau[0] == plateau[4] == plateau[8] != "_":
        rep = plateau[0]
    elif plateau[2] == plateau[4] == plateau[6] != "_":
        rep = plateau[2]

    return rep

def getNomGagnant(signe: str, j1: dict,j2: dict)-> str:
    rep = ""
    if j1.get(SIGNE) == signe:
        rep = j1.get(NAME)
    if j2.get(SIGNE) == signe:
        rep = j2.get(NAME)

    return rep


