from Constante import *
def CreerJoueur()->dict:
    j = {}
    a = input("Entrez un pseudo : ")

    j[NAME] = a
    j[SIGNE] = ""

    return j

def setSigneJoueurs(j1 : dict,j2 : dict)->dict:

    j1[SIGNE] = "X"
    j2[SIGNE] = "O"

    return j1, j2

def rotationJoueurs(j1: dict, j2: dict)-> list:
    joueur = []
    joueur.append(j1)
    joueur.append(j2)

    return joueur

def switchJoueurs(joueur: list)->list:

    joueur.append(joueur[0])
    del joueur[0]

    return joueur